<!-- VENDOR CSS -->
    <link
      rel="stylesheet"
      href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}"
    />
    <link
      rel="stylesheet"
      href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}"
    />
    <link rel="stylesheet" href="{{ asset('assets/vendor/linearicons/style.css') }}" />
    <link
      rel="stylesheet"
      href="{{ asset('assets/vendor/chartist/css/chartist-custom.css') }}"
    />
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}" />
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="{{ asset('assets/css/demo.css') }}" />
    <!-- GOOGLE FONTS -->
    <link
      href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700"
      rel="stylesheet"
    />