<!-- NAVBAR -->
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="brand" style="background-color: #30aee4; width: 260px;">
          <a href="{{ route('sampah.index') }}"
            ><img
              src="{{ url('images/logow4c.png') }}"
              alt="w4c"
              class="img-responsive logo center-block"
          /></a>
        </div>
        <div class="container-fluid">
          <div class="navbar-btn">
            <button type="button" class="btn-toggle-fullwidth">
              <img src="{{ url('images/Vector.png') }}" alt="" />
            </button>
          </div>
        </div>
      </nav>
      <!-- END NAVBAR -->