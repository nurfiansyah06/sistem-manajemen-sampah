<!-- LEFT SIDEBAR -->
      <div id="sidebar-nav" class="sidebar">
        <div class="sidebar-scroll">
          <nav>
            <ul class="nav">
              <li>
                <a href="#subPages" data-toggle="collapse" class="collapsed">
                  <span>Sampah</span>
                  <i class="icon-submenu lnr lnr-chevron-left"></i
                ></a>
                <div id="subPages" class="collapse">
                  <ul class="nav">
                    <li>
                      <a href="{{ route('sampah.index') }}" class=""><i class="fa fa-circle-o"></i> Tambah Sampah</a>
                    </li>
                    <li><a href="{{ route('sampah.index') }}" class=""><i class="fa fa-dot-circle-o"></i> Semua Sampah</a></li>
                  </ul>
                </div>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <!-- END LEFT SIDEBAR -->