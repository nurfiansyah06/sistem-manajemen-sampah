<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Sistem Manajemen Sampah</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
    />
    @include('includes.style')
  </head>

  <body>
    <!-- WRAPPER -->
    <div id="wrapper">
      
        @include('includes.navbar')
      
        @include('includes.sidebar')
      <!-- MAIN -->
      <div class="main">
        @yield('content')
	</div>

	<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
        <div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Tambah Sampah</h3>
			</div>
			<div class="panel-body">
        <form action="{{ route('sampah.store') }}" method="post">
          @csrf
          
          <div class="form-group {{ $errors->has('nama') ? ' has-error' : '' }}">
            <label for="namasampah"><small>Nama Sampah</small></label>
				    <input type="text" class="form-control"  name="nama" value="{{ old('nama') }}" placeholder="Nama Sampah">
            @if ($errors->has('nama'))
              <span class="help-block">{{ $errors->first('nama') }}</span>
            @endif
          </div>
        
          <br>
        
          <div class="form-group {{ $errors->has('kategori') ? ' has-error' : '' }}">
            <label for="kategorisampah"><small>Kategori Sampah</small></label>
				    <select class="form-control" name="kategori">
              <option value="">Kategori Sampah</option>
              <option value="Plastik {{ (old('kategori') == 'Plastik') ? ' selected ' : ''  }}">Plastik</option>
              <option value="Logam {{ (old('kategori') == 'Logam') ? ' selected ' : ''  }}">Logam</option>
              <option value="Kertas {{ (old('kategori') == 'Kertas') ? ' selected ' : ''  }}">Kertas</option>
            </select>
            @if ($errors->has('kategori'))
              <span class="help-block">{{ $errors->first('kategori') }}</span>
            @endif
          </div>
				<br>
				<div class="foot-modal text-right">
					<button type="button" class="btn btn-secondary" style="margin-right:10px" data-dismiss="modal">Close</button>
        	<button type="submit" class="btn btn-primary">Tambah</button>
				</div>
        </form>
			</div>
		</div>
  </div>
</div>
        <!-- END MAIN CONTENT -->
      </div>
      <!-- END MAIN -->
      <div class="clearfix"></div>
     
    </div>
    <!-- END WRAPPER -->
    @include('includes.script')
  </body>
</html>
