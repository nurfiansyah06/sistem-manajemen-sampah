@extends('layouts.main')

@section('content')
    <!-- MAIN CONTENT -->
        <div class="main-content">
          <div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title">SAMPAH</h3>
            </div>
          </div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-10">
						<h3 class="panel-title">Semua Sampah</h3>
					</div>
					<div class="col-lg-2">
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
							<i class="lnr lnr-plus-circle"></i> Tambah</button>
					</div>
				</div>
				<br>
				<div class="panel">
					<table class="table">
						<thead style="background-color:#E2E2E2">
							<tr>
								<th style="color: black">#</th>
								<th>
									@sortablelink('nama')
									<img src="{{ url('images/Vector (1).png') }}" alt="" srcset="">
								</th>
								<th>
									@sortablelink('kategori','Jenis Sampah')
									<img src="{{ url('images/Vector (1).png') }}" alt="" srcset="">
								</th>
								<th style="color: black">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td>
									<form action="{{ route('sampah.index') }}" method="get">
										@csrf
										<input class="form-control" type="text" name="carinama" id="" placeholder="cari" style="width: 160px;">
									</form>
								</td>
								<td>
									<form action="{{ route('sampah.index') }}" method="get">
										@csrf
										<input class="form-control" type="text" name="carikategori" id="" placeholder="cari" style="width: 160px;">
									</form>
								</td>
								<td></td>
                            </tr>
                            @foreach ($sampah as $item)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $item->nama }}</td>
								<td>{{ $item->kategori }}</td>
								<td>
                                    <form action="{{ route('sampah.destroy',$item->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-link btn-xs" style="color: red">
                                            Hapus
                                        </button>
                                    </form>
                                </td>
							</tr>
                            @endforeach
						</tbody>
					</table>
				</div>
				<div class="panel" style="height: 80px;">
					<div class="panel-heading">
						<div class="row">
							<div class="col-lg-9">
								<p>Menampilkan {{ $sampah->count() }} dari {{ $sampah->total() }}</p>
							</div>
							<div class="col-lg-3">
								{{ $sampah->links() }}
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
@endsection