<?php

namespace App;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;

class Sampah extends Model
{

    use Sortable;
    protected $table = 'sampah';

    protected $fillable = [
        'nama','kategori'
    ];

    public $sortable = ['id','nama','kategori'];
}
