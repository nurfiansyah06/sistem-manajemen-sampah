<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Sampah;

class SampahController extends Controller
{

    
    public function all(Request $request)
    {
        $id = $request->input('id');
        $limit = $request->input('limit',10);
        $nama = $request->input('nama');
        $kategori = $request->input('kategori');


        if($id)
        {
            $sampah = Sampah::find($id);

            if ($sampah)
                return ResponseFormatter::success($sampah,'Data berhasil diambil');
            else
                return ResponseFormatter::error(null, 'Data tidak ada','404');
        }

        

        if($nama)
        {
            $sampah = Sampah::where('nama','like','%'.$nama.'%')->get();
            
            if ($sampah)
                return ResponseFormatter::success($sampah,'Data berhasil diambil');
            else
                return ResponseFormatter::error(null, 'Data tidak ada','404');
        }    

        if($kategori)
        {
            $sampah = Sampah::where('kategori','like','%'.$kategori.'%')->get();
            
            if ($sampah)
                return ResponseFormatter::success($sampah,'Data berhasil diambil');
            else
                return ResponseFormatter::error(null, 'Data tidak ada','404');
        }
        
        
        $sampah = Sampah::paginate($limit);

        return ResponseFormatter::success(
            $sampah,'Data sampah berhasil diambil'
        );
    }


    public function store(Request $request)
    {

        $validated = $request->validate([
            'nama' => 'required|max:255',
            'kategori' => 'required|string|in:Logam,Kertas,Plastik',
        ]);

        $sampah = Sampah::create ([
            'nama' => $validated['nama'],
            'kategori' => $validated['kategori'],
        ]);

       return ResponseFormatter::success($sampah, 'Data berhasil dimasukkan');
    }

    public function destroy($id)
    {
        $sampah = Sampah::destroy($id);

        if ($sampah)
            return ResponseFormatter::success($sampah,'Data berhasil dihapus');
        else
            return ResponseFormatter::error(null, 'Data tidak ada','404');
    }
}
