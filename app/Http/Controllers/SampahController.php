<?php

namespace App\Http\Controllers;
use App\Sampah;
use Illuminate\Http\Request;

class SampahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('carinama')) {
            $sampah = Sampah::where('nama','LIKE','%'.$request->carinama.'%')->paginate(10);    
        } elseif ($request->has('carikategori')) {
            $sampah = Sampah::where('kategori','LIKE','%'.$request->carikategori.'%')->paginate(10);
        }
        else {
            $sampah = Sampah::sortable()->paginate(10);
        }

        return view('home', [
            'sampah' => $sampah
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama' => 'required|max:255',
            'kategori' => 'required|string|in:Logam,Kertas,Plastik',
        ]);

        $sampah = Sampah::create ([
            'nama' => $validated['nama'],
            'kategori' => $validated['kategori'],
        ]);

        return redirect()->route('sampah.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sampah = Sampah::destroy($id);

        return redirect()->back();
    }

    public function sortByName(Request $request)
    {
        $sampah = Sampah::orderBy('nama','desc')->paginate(10);

        return view('home', [
            'sampah' => $sampah
        ]);
    }
    
}
